import _ from 'lodash'

/**
 * Function to calculate the frecuency of n-grams of a given length.
 * (Cryptanalysis)
 * @param {String} source 
 * @param {Integer} long 
 * @return {Object} map of frecuency of ngrams.
 */
export default function ngramer(source, size) {
  let plaintext = source.toUpperCase().replace(/[^A-Z ]/g, '')
  let occurances = {}
  let words = plaintext.split(/\s+/)

  // Calculate occurances
  words.map(word => {
    for (let start=0; start+size <= word.length; start++) {
      let pair = word.substring(start, start+size)
      if (occurances[pair] == undefined) {
        occurances[pair] = 1
      } else {
        occurances[pair] += 1 
      }
    }
  });

  return occurances
}