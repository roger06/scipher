import fs from 'fs'
import _ from 'lodash'

/**
 * Helper to calculate the score of a decoded text 
 * based on a given frequency analysis.
 */
class Scorer {
  constructor(ngrams=false) {
    this.ngrams = {}
    if (ngrams) {
      this.ngrams = this._getScore(ngrams)
    }
  }

  /**
   * Load a json a source for the frecuency analysis.
   * @param {String} filePath 
   */
  loadFromFile(filePath) {
    let data = JSON.parse(fs.readFileSync(filePath, 'utf8'))
    this.ngrams = this._getScore(data)
  }

  /** 
  *  Assigns to each charged ngram the probability of 
  *  occurrence that it possesses based on the analysis of frequencies.
  *  @param {Object} ngram mapper (frequencies analysis)
  *  @return {Object} ngram probability mapper
  */
  _getScore(frequencies) {
    this.nValue = Object.keys(frequencies)[0].length
    let ngramTotal  = _.sum(_.values(frequencies))
    let ngrams = {}
    //calculate log probabilities
    _.forEach(frequencies, (value, key) => {
      //TODO: Improve this with log probabilities
      ngrams[key] = parseFloat(value) / ngramTotal 
    })
    
    return ngrams
  }

  /**
   * Calculates the score of the given text based on the frequency analysis loaded.
   * @param {String} text
   * @return {Float} the score obtained
   */
  calculate(text) {
    let score = 0
    let textLength = text.length - this.nValue + 1

    for(let i=0; i < textLength; i++) {
      let sbstr = text.substring(i, i + this.nValue)
      
      //TODO: Calculate a minimum value for ngrams not found
      if (_.has(this.ngrams, sbstr)) {
        score += this.ngrams[sbstr]
      }
    }

    return score;
  }

}

export default Scorer