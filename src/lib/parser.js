import _ from 'lodash'

/**
 * Helper module to perform operations on text
 */
export default {
  /**
  * Generates a random alphabet.
  * @return {String} alphabet
  */
  generateRandomAlphabet() {
    let keychars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    return _.shuffle(keychars).join("")
  },

  /**
   * Retrieve a random fragment of text with the given number of lines.
   * @param {String} source 
   * @param {Integer} linesNumber 
   * @return {String} the fragment of text.
   */
  extractFragment(source, linesNumber, maxChars=false) {
    let textLines  = source.replace(/(^[ \t]*\n)/gm, "").split('\n')
    let totalLines = textLines.length
    
    // If the text has fewer lines than the requested limit, it returns the full text
    if (linesNumber > totalLines ) {
      return source
    }

    textLines = textLines.slice(0, linesNumber - 1).join('\n')
    
    if (maxChars) {
      return textLines.substring(0, maxChars)
    }

    return textLines
  },

  /**
   * Replace all characters in the source with the fiven key.
   * @param {String} source 
   * @param {String} key 
   * @return {String} Decode text
   */
  decode(source, alphabetKey) {
    let ciphertext = source.toUpperCase()
    let key        = alphabetKey.toUpperCase().replace(/[^A-Z]/g, "")

    //We check that the key is valid.
    if (key.length != 26 ) {
      throw new Error('The alphabet key is invalid.')
    }

    if (ciphertext.length < 1) {
      return ciphertext
    }

    let plaintext= "",
        re       = /[A-Z]/

    for(let i=0; i < ciphertext.length; i++) { 
      if(re.test(ciphertext.charAt(i))) {
        plaintext += String.fromCharCode(key.indexOf(ciphertext.charAt(i))+65)
      } else {
        plaintext += ciphertext.charAt(i)
      } 
    } 
    
    return plaintext
  },

  /**
   * Replace all characters in the source with the fiven key.
   * @param {String} source 
   * @param {String} key 
   * @return {String} Encoded text
   */
  encode(source, alphabetKey) {
    let plaintext = source.toUpperCase();  
    let key        = alphabetKey.toUpperCase().replace(/[^A-Z]/g, "")

    //We check that the key is valid.
    if (key.length != 26 ) {
      throw new Error('The alphabet key is invalid.')
    }

    if (plaintext.length < 1) {
      return plaintext
    }

    let ciphertext= "",
    re       = /[A-Z]/

    for(let i=0; i < plaintext.length; i++) { 
        if(re.test(plaintext.charAt(i))) {
          ciphertext += key.charAt(plaintext.charCodeAt(i)-65)
        } else {
          ciphertext += plaintext.charAt(i); 
        }
    } 

    return ciphertext
  }
}