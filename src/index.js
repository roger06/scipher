import fs from 'fs'
import path from 'path'
import _ from 'lodash'
import ngramer from './lib/ngramer'
import parser from './lib/parser'
import Scorer from './lib/scorer'

module.exports = {
  /**
   * Try to break a given ciphertext.
   * @param {String} source
   * @param {Object} ngrams
   * @param {Integer} minIteration
   * @param {Float} minRate
   * @param {Function} onIteration
   * @return {Object} Best result posible.
   */
  breaktext: (source, ngrams, minIterations=500, minRate=70 , onIteration=false) => {
    let scorer = new Scorer(ngrams),
        results     = {},
        testedKeys  = [],
        currentKey  = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
        iteration   = 0,
        currentRate = 0,
        maxRate     = 0

    while (maxRate < minRate && iteration < minIterations) {
      iteration++
      currentKey = _.shuffle(currentKey).join("")
      while(testedKeys.indexOf(currentKey) > -1) {
        currentKey = _.shuffle(currentKey).join("")
      }
      
      testedKeys.push(currentKey)

      let deciphered = parser.decode(source, currentKey)
      currentRate    = scorer.calculate(deciphered)

      let count = 0
      // Iterate over the current key swaping 2 letter.
      while(count < 100) {
        let newKey = currentKey.split("")
        let a = _.random(0, 25);
        let b = _.random(0, 25);
        
        // Swap two letters in the key.
        let aux = newKey[a]
        newKey[a] = newKey[b]
        newKey[b] = aux
        newKey = newKey.join("")

        while(testedKeys.indexOf(newKey) > -1) {
          a = _.random(0, 25)
          b = _.random(0, 25)
          
          newKey = newKey.split("")
          aux = newKey[a]
          newKey[a] = newKey[b]
          newKey[b] = aux
          newKey = newKey.join("")
        }

        testedKeys.push(newKey)

        let deciphered = parser.decode(source, newKey)
        let newRate    = scorer.calculate(deciphered)
        
        // if the rate obtained with the swipe was better, replace the current with it
        if (newRate > currentRate) {
          currentRate = newRate
          currentKey  = newKey
          count = 0
        }
        count++
      }

      // Keep track of best score seen so far
      if (currentRate > maxRate) {
        maxRate = currentRate
        deciphered = parser.decode(source, currentKey)
        
        results = {
          iteration : iteration,
          score     : currentRate,
          key       : currentKey,
          plaintext : deciphered,
          testedKeys: testedKeys.length,
        }

        if (onIteration) {
          onIteration(results)
        }
      }
    }

    return results
  },

  /**
   * Replace all characters in the source with the fiven key.
   * @param {String} source 
   * @param {String} key 
   * @return {String} Decoded text
   */
  decode: parser.decode,

  /**
   * Replace all characters in the source with the fiven key.
   * @param {String} source 
   * @param {String} key 
   * @return {String} Encoded text
   */
  encode: parser.encode,

  extractFragment: parser.extractFragment,

  ngrams: ngramer,
}