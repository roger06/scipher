# Simple Sustition Cipher

(This module was made as a practice for WhitePrompt_.)

For the decoding of the siple substitution cipher, I chose the use of a hill-climbing algorithm to find the best possible key.

To determine how good is the founded key, a measure of aptitude is used between the text obtained by deciphering the ciphertext with each key and the frequency analysis of ngrams in normal English text.

The algorithm used has the following steps:
- Generate a random key (currentKey), decipher the ciphertext using this key. Rate the fitness of the deciphered text, store the result.
- Change the key (swap two letters in the key at random), measure the fitness of the deciphered text using the new key (newKey).
- If the fitness is higher with the modified key, discard our old currentKey and store the modified key as the new currentKeyt.
- Repeat the swaping of letters, unless the improvement in fitness occurred in the last 100 iterations.

This module offers the ability to approach the decipherment of a text using the most common English trigrams (loaded in this project)or perform the ngrams frequency analysis of a given plain text for this purpose.

The delivered files are located in the Examples folder.
To run the solution, first clone this repo and :

```bash
$ yarn  # Install project dependencies (or `npm install`)
$ yarn link #  Create a symlink in the global folder (or `npm link`)
$ cd example
$ scipher --help # Optional to show help
$ scipher ./encrypted.txt # For more options read the documentation below.
```

## Requirements
* node `^5.0.0`
* yarn `^0.23.0` or npm `^3.0.0`

## Installation

Install the project dependencies. It is recommended that you use [Yarn](https://yarnpkg.com/) for deterministic dependency management, but `npm install` will suffice.

```bash
$ yarn  # Install project dependencies (or `npm install`)
```

## Scripts

|`yarn <script>`    |Description|
|-------------------|-----------|
|`build`            |Builds the application to ./dist|
|`test`             |Runs unit tests with Karma. See [testing](#testing)|
|`test:watch`       |Runs `test` in watch mode to re-run tests when changed|


## CLI 
Para usar los comandos en la teminal, se debe vincular el paquete.

```bash
$ yarn link #  Create a symlink in the global folder (or `npm link`)
```

For help
```bash
$ scipher --help 
```



## Usage 

```bash
$ scipher [source] [options]
```

|`options`    |Description|
|-------------------|-----------|
|`-o`               |Output file name [./plaintext.txt]|
|`-i`               |Max number of iterations [300]|
|`-s`               |Min score required 0..1 [1]|
|`-n`               |Path to ngram files|
|`-l`               |Number of lines to test [50]|
|`-p`               |Show decoded lines [true]|


## Project Structure

The project was made as generic npm module. With a cli interface for better work.
But can be used as external library in any project.

```
.
├── bin                      # Bin scripts to execute the scipher from the terminal
├── data                     # Generic static files (most common ngrams of English)
├── example                  # Folder with files delivered for demonstration
│   ├── encrypted.txt        # Case 1 (simple).      
│   ├── encrypted_hard.txt   # Case 2 (hard).
│   └── plain.txt            # Plain example text.
├── src                      # Application source code
│   ├── main.js              # Module
│   ├── lib                  # Helpers
│   │   ├── ngramer.js       # Function to calculate the frecuency of n-grams of a given length.
│   │   ├── parser.js        # Helpsers functions to work with the text
│   │   └── scorer.js        # Helper to calculate the score of a decoded text 
└── tests                    # Unit tests
```


## TODO

- Optimize score collection.
- Use a child process to run the breaker
- Possibility of obtaining a plaintext with a process finished by the user