#!/usr/bin/env node
'use strict';

var scipher = require('../dist/index'),
    fs = require('fs'),
    clc  = require('cli-color'),
    temporalResult = {},
    argv = require('optimist').argv;

if (argv.h || argv.help) {
  console.log([
    'Usage: scipher [path] [options]',
    '',
    'Options:',
    '  -o           Output file name [./plaintext.txt]',
    '  -i           Max number of iterations [300]',
    '  -s           Min score required 0..1 [1]',
    '  -n           Path to ngram files [true]',
    '  -l           Number of lines to test [50]',
    '  -p           Show decoded lines [true]'
  ].join('\n'));
  process.exit();
}

var sourcePath = argv._[0],
    output     = argv.o || './plaintext.txt',
    iterations = argv.i || 300,
    minScore   = argv.s || 1,
    ngramsPath = argv.n || false,
    lines      = argv.l || 50,
    showPlain  = argv.p || true;

if (!sourcePath) {
  throw new Error('You must specify the file to be decoded');
  process.exit(1);
}

var sourceFile = fs.readFileSync(sourcePath, 'utf8').toString();
var source = scipher.extractFragment(sourceFile, lines);

if (!ngramsPath) {
  var ngrams = JSON.parse(fs.readFileSync('./data/triagrams.json', 'utf8'));
} else {
  var ngramFile = fs.readFileSync(ngramsPath).toString();
  var ngrams = scipher.ngrams(ngramFile, 3);
}

var result = scipher.breaktext(source, ngrams, iterations, minScore, showResults);
var decoded = scipher.decode(sourceFile, result.key);
fs.writeFileSync(output, decoded);
process.exit();

function showResults(results) {
  var numberOfLines = showPlain ? -6 : -5;
  var percent = (results.iteration / iterations) * 100;
  var empty = Array(100 - parseInt(percent)).join("-");
  var complete = Array(parseInt(percent)).join("=");

  var message = clc.cyan("Iterations completes:") +"    ["+ complete + empty +"]" + "  " + percent.toFixed(2) + '% \n'
              + clc.cyan("Iteration")+"                "+ results.iteration + '\n'
              + clc.cyan("Best Score")+"               "+ results.score +"\n"
              + clc.cyan("Number of keys tested")+"    "+ results.testedKeys +"\n"
              + clc.cyan("Best Key")+"                 "+ results.key + "\n";
            
  if (numberOfLines) {
    message += clc.cyan("Plain Text")+"               "+ results.plaintext.replace(/\n|\r/g, " ").substring(0, 70)   +".... \n";
  }

  process.stdout.clearLine();
  if (results.iteration != 1) {
    process.stdout.moveCursor(0, -1 * message.toString().split('\n').length + 1);
  }

  process.stdout.write(message)
}
