/*eslint no-console: 0*/
'use strict';
import scipher from '../src/index'
import fs from 'fs'

describe('Main', () => {

  it('test test', () => {
    let ngrams = JSON.parse(fs.readFileSync('./data/triagrams.json', 'utf8'));
    let ciphertext = "Puz mplutuo, gyzu Olzopl Dbmdb gpcz wlpm alpskhzr rlzbmd, yz wpsur ytmdzhw albudwplmzr tu ytd kzr tuap b yplltkhz nzlmtu.  Yz hbf pu ytd blmpsl-htcz kbqc, bur tw yz htwazr ytd yzbr b htaahz yz qpshr dzz ytd klpgu kzhhf, dhtoyahf rpmzr bur rtntrzr kf blqyzd tuap datww dzqatpud.  Ayz kzrrtuo gbd yblrhf bkhz ap qpnzl ta bur dzzmzr lzbrf ap dhtrz pww buf mpmzua.  Ytd mbuf hzod, vtatwshhf aytu qpmvblzr gtay ayz dtez pw ayz lzda pw ytm, gbnzr bkpsa yzhvhzddhf bd yz hppczr."
    let result = scipher.breaktext(ciphertext, ngrams, 5, 1)
  });
});