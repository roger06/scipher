/*eslint no-console: 0*/
'use strict';
import ngramer from 'lib/ngramer'

describe('ngramer', () => {
  it('should get all possible ngrams in one word', () => {
    const text = 'TWO WORDS'

    //Manual bigrams => ["tw", "wo", "wo", "or", "rd", "ds"]
    let manualResult = {
      "TW": 1,
      "WO": 2,
      "OR": 1,
      "RD": 1,
      "DS": 1
    }
    let result = ngramer(text, 2)
    expect(result).toEqual(manualResult);

    //Manual trigrams => ["two", "wor", "ord", "rds"]
    manualResult = {
      "TWO": 1,
      "WOR": 1,
      "ORD": 1,
      "RDS": 1
    }
    result = ngramer(text, 3)
    expect(result).toEqual(manualResult);
  })
})