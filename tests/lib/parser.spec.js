/*eslint no-console: 0*/
'use strict';
import parser from 'lib/parser'

describe('parser', () => {
  it('generateRandomAlphabet :: should generate a random alphabet', () => {
    let newAlphabet = parser.generateRandomAlphabet()
    expect(newAlphabet.length).toEqual(26)
  })

  it('extractFragment :: should generate a new text with the given number of lines', ()=> {
    const text = "Line1 \n Line2 \n Line3 \n Line4"

    let newText = parser.extractFragment(text, 1)
    expect(newText.split('\n').length).toEqual(1)
  })

  it('extractFragment :: should return the original text if the source text lines are less than the requested ones', ()=> {
    const text = "Line1 \n Line2 \n Line3 \n Line4"

    let newText = parser.extractFragment(text, 5)
    expect(newText).toEqual(text)
  })

  it('extractFragment :: should return a limited version of the text, if the maxChars is given', ()=> {
    const text = "Line1 \n Line2 \n Line3 \n Line4"

    let newText = parser.extractFragment(text, 4, 10)
    expect(newText.length).toEqual(10)
  })

  it('encode :: should generate a enocded version of the text', () => {
    const randomAlphabet = parser.generateRandomAlphabet()
    const text = "HELLO WORLD"

    let newText = parser.encode(text, randomAlphabet)

    expect(text.length).toEqual(newText.length)
    expect(parser.decode(newText, randomAlphabet)).toEqual(text)
  })

  it('should fail if the given alphabet is wrong', () => {
    const text = "HELLO WORLD"
    let randomAlphabet = parser.generateRandomAlphabet()

    //Remove a single letter to break the valid alphabet
    randomAlphabet = randomAlphabet.slice(0, randomAlphabet.length - 1)

    expect(() => {
      let newText = parser.encode(text, randomAlphabet)
    }).toThrow();

    expect(() => {
      let newText = parser.decode(text, randomAlphabet)
    }).toThrow();
  })
})