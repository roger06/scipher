/*eslint no-console: 0*/
'use strict';
import Scorer from 'lib/scorer'

describe('scorer', () => {
  it('should return 0 if no coincides are founded', () => {
    const text = "one two three"

    //We use trigrams to test.
    const testNgram = {
      "aaa": 1,
      "bbb": 2,
      "ccc": 3
    }

    const scorer = new Scorer(testNgram)
    
    let value = scorer.calculate(text)

    expect(value).toEqual(0)
  })

  it('should return a value different of 0 if coincides are founded', () => {
    const text = "one two three"

    //We use trigrams to test.
    const testNgram = {
      "one": 1, //match trigram
      "bbb": 2,
      "ccc": 3
    }

    const scorer = new Scorer(testNgram)    
    let value = scorer.calculate(text)

    expect(value).not.toEqual(0)
  })
})